#include <stdio.h>
#include <stdlib.h>
#include "Cartas.h"
//Estructura para lista doblemente enlazada
typedef struct nodo
{
    int dato;
    struct nodo *sig;
    struct nodo *ant;
    
}nodo_lista;

//Funciones Utilizadas
void iniciarLista();
void insertarCarta(int num);
void mostrarLista();
void mostrarActual();
void moverDerecha();
void moverIzquierda();
void swap( int* a, int* b );
nodo_lista* particion(nodo_lista* a, nodo_lista* b);
void quickSort(nodo_lista *a, nodo_lista *b);
nodo_lista* particiondesorden(nodo_lista* a, nodo_lista* b);
void desordenar(nodo_lista *a, nodo_lista *b);
void menu();
void CartaValor(int valor);



//Se inicia la lista con sus punteros en primer y ultimo.
nodo_lista* primero = NULL;
nodo_lista* ultimo = NULL;
nodo_lista* Head= NULL;


//Función principal
int main(){ 
    
    iniciarLista();
    menu();
    return 0;
}
//Iniciar Lista
void iniciarLista(){
    int i; // Variable para el for
    // Agrega las "cartas" al arreglo
    for(i = 1; i < 53; i++){
        insertarCarta(i);
    }

}

// Se agrega un nuevo nodo a la lista
void insertarCarta(int num){

	nodo_lista *nuevo =(nodo_lista*)malloc(sizeof(nodo_lista));
	nuevo->dato=num;
	//Esta vacía la lista
	if (Head == NULL){

		Head=nuevo;
		Head->sig = NULL;
		Head->ant = NULL;
		ultimo=Head;
        primero=Head;
    }
	//Hay un elemento ya en la lista
	else
	{
		ultimo->sig=nuevo;
		nuevo->sig=NULL;
		nuevo->ant=ultimo;
		ultimo=nuevo;
	}

}

//Muestra la lista completa doblemente enlazada
void mostrarLista(){

	nodo_lista *actual = (nodo_lista*)malloc(sizeof(nodo_lista));
	actual = Head;
    if (Head!=NULL)
	{
		int indice=1;
		while (actual != NULL)
		{
			
            CartaValor(actual->dato);
       

			actual = actual->sig;
			indice = indice+1;
		}
			
	}
	else
	{
		printf("\n Lista Vacía");
	}	
}

//Muestra la carta actual
void mostrarActual(){
    nodo_lista *actual = (nodo_lista*)malloc(sizeof(nodo_lista));
    actual=primero;
    CartaValor(actual->dato);
}

//Mueve el puntero y muestra la carta siguiente de la actual
void moverDerecha(){

    
    nodo_lista *actual = (nodo_lista*)malloc(sizeof(nodo_lista));
    actual = primero;
        if (actual->sig!=NULL)
        {
            actual = actual->sig;
            CartaValor(actual->dato);
          
            primero=actual;
        }
        else
        {
            printf("\n Fin de la Baraja ");
        }
    
}
//Mueve el puntero y muestra la carta anterior de la actual
void moverIzquierda(){
    nodo_lista *actual = (nodo_lista*)malloc(sizeof(nodo_lista));
    actual= primero;

    if (actual->ant!=NULL){

        actual= actual->ant;   
        CartaValor(actual->dato);
        primero=actual;
        
        }
        else{
		printf("\n Fin de la Baraja ");
	    }
}



//Intercambia una posicion con otra en la lista
void swap( int* a, int* b ) {   
    int tmp = *a;      *a = *b;       *b = tmp;   
} 
//separa la lista en menores, iguales y mayores a partir del pivote
nodo_lista* particion(nodo_lista* a, nodo_lista* b){
    //seleccionando pivote será el ultimo elemento de la lista
    int pivote = b->dato;

    nodo_lista *ind = a->ant;

    for(nodo_lista *j = a; j != b; j = j->sig){
        //se mueven menores o iguales a pivote mediante swap
        if(j->dato <= pivote){
            //mueve el indice una posicion
            ind = ( ind == NULL)? a : ind->sig;
            //intercambia los nodos
            swap(&(ind->dato), &(j->dato));
        }
    }
    //mueve el indice una posicion
    ind = ( ind == NULL)? a : ind->sig;
    swap(&(ind->dato), &(b->dato));
    return ind;
}
//quicksort que retorna a sí mismo para hacer diversas particiones
void quickSort(nodo_lista *a, nodo_lista *b) {

    if (b != NULL && a != b && a != b->sig){ 

        nodo_lista *part = particion(a, b); 
        //esto hace las subdivisiones del quick
        quickSort(a, part->ant); 
        quickSort(part->sig, b); 
    } 

}
// Crea divisiones de la lista, mayores, menores e iguales
nodo_lista* particiondesorden(nodo_lista* a, nodo_lista* b){
    // Se crea el pivote como un entero 0
    int pivote = 0;
    
    nodo_lista *indice = a->ant;
    // Se crea la semilla para el random
    srand(time(0));
    

    int i = 0;
    for(nodo_lista *j = a; j != b; j = j->sig){
        //se mueven menores o iguales a pivote mediante swap
        pivote = rand() % 5; // Crea random diferentes por cada ejecución del for
        if(j->dato <= pivote*rand()){
            // Se mueve el indice una posicion
            indice = ( indice == NULL)? a : indice->sig;

            // LLama sl swap para intercambiar posicones
            swap(&(indice->dato), &(j->dato));
        }
        i++;
    }
    // Se mueve el indice una posicion
    indice = ( indice == NULL)? a : indice->sig;
    swap(&(indice->dato), &(b->dato));
    return indice;
}
// Función para desordenar la lista que realiza las particiones.
void desordenar(nodo_lista *a, nodo_lista *b) {
    

    if (b != NULL && a != b && a != b->sig){ 

        nodo_lista *division = particiondesorden(a, b);

        // Realiza las particiones
        desordenar(a, division->ant); 
        desordenar(division->sig, b); 
    } 

}
//Menú que controla todas las funciones
void menu(){
    int opcion;
    char temp[500];
    
    do{
        printf("\n");
        printf("\n+---+---------------------------------+\n");
        printf("\n|            Opción                   |\n");   
        printf("\n+---+---------------------------------+\n");
        printf("\n| 1 | Mostrar actual                  |\n");
        printf("\n| 2 | Mostrar Baraja Completa.        |\n");
        printf("\n| 3 | Moverse a la Carta Siguiente.  |\n");
        printf("\n| 4 | Moverse a la Carta Anterior.    |\n");
        printf("\n| 5 | Barajar la baraja.              |\n");
        printf("\n| 6 | Ordenar.                        |\n");
        printf("\n| 7 | Salir.                          |\n");
        printf("\n+---+---------------------------------+\n");
        printf("\n");
        scanf("%d",&opcion);
        fgets(temp,500,stdin);
        
        switch (opcion)
        {
        case 1:
                
            mostrarActual();
            break;
            
        case 2:
            
            mostrarLista();
            break;

        case 3:
            moverDerecha();
            break;

        case 4:
            moverIzquierda();
            break;
            
        case 5:

            desordenar(Head,ultimo);
            primero=Head;
            break;
            
        case 6:
                
            quickSort(Head,ultimo);
            primero=Head;
            break;

        case 7:
            break;    
            
        default:
            printf("Ingrese un número del 1 al 7");
            break;
            }    
    }while (opcion != 7);
}

//Se asigna el valor de cada carta a un número
void CartaValor(int valor){

    switch (valor)
    {
    case 1:
        printf(BastosA);
        break;
    
    case 2:
        printf(Bastos2);
        break;
    
    case 3:
        printf(Bastos3);
        break;
    
    case 4:
        printf(Bastos4);
        break;
    
    case 5:
        printf(Bastos5);
        break;
    
    case 6:
        printf(Bastos6);
        break;    
   
    case 7:
        printf(Bastos7);
        break;    
    
    case 8:
        printf(Bastos8);
        break;
    
    case 9:
        printf(Bastos9);
        break;
    
    case 10:
        printf(Bastos10);
        break;
    
    case 11:
        printf(BastosJ);
        break;
    
    case 12:
        printf(BastosQ);
        break;
    
    case 13:
        printf(BastosK);
        break;
    
    case 14:
        printf(CorazonesA);
        break;
    
    case 15:
        printf(Corazones2);
        break;
    
    case 16:
        printf(Corazones3);
        break;
    
    case 17:
        printf(Corazones4);
        break;
    
    case 18:
        printf(Corazones5);
        break;
    
    case 19:
        printf(Corazones6);
        break;    
   
    case 20:
        printf(Corazones7);
        break;    
    
    case 21:
        printf(Corazones8);
        break;
    
    case 22:
        printf(Corazones9);
        break;
    
    case 23:
        printf(Corazones10);
        break;
    
    case 24:
        printf(CorazonesJ);
        break;
    
    case 25:
        printf(CorazonesQ);
        break;
   
    case 26:
        printf(CorazonesK);
        break;
    
    case 27:
        printf(DiamantesA);
        break;
    
    case 28:
        printf(Diamantes2);
        break;
    
    case 29:
        printf(Diamantes3);
        break;
    
    case 30:
        printf(Diamantes4);
        break;
    
    case 31:
        printf(Diamantes5);
        break;
    
    case 32:
        printf(Diamantes6);
        break;
    
    case 33:
        printf(Diamantes7);
        break;
    
    case 34:
        printf(Diamantes8);
        break;    
   
    case 35:
        printf(Diamantes9);
        break;    
    
    case 36:
        printf(Diamantes10);
        break;
    
    case 37:
        printf(DiamantesJ);
        break;
    
    case 38:
        printf(DiamantesQ);
        break;
    
    case 39:
        printf(DiamantesK);
        break;
    
    case 40:
        printf(TrebolesA);
        break;
    
    case 41:
        printf(Treboles2);
        break;
    
    case 42:
        printf(Treboles3);
        break;
    
    case 43:
        printf(Treboles4);
        break;
    
    case 44:
        printf(Treboles5);
        break;
    
    case 45:
        printf(Treboles6);
        break;
    
    case 46:
        printf(Treboles7);
        break;
    
    case 47:
        printf(Treboles8);
        break;
    
    case 48:
        printf(Treboles9);
        break;
    
    case 49:
        printf(Treboles10);
        break;    
   
    case 50:
        printf(TrebolesJ);
        break;    
    
    case 51:
        printf(TrebolesQ);
        break;
    
    case 52:
        printf(TrebolesK);
        break;

    default:
        printf(" ");
        break;
    }


}