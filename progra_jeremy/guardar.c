//https://www.geeksforgeeks.org/quicksort-for-linked-list/
// Quicksort 

#include <stdio.h>;
#include <stdlib.h>
  
// nodo doblemente enlazado

typedef struct nodo
{
    int dato;
    struct nodo *sig;
    struct nodo *ant;
    
}nodo_lista;

nodo_lista* primero = NULL;
nodo_lista* ultimo = NULL;
 
 int main(){

	insertarCarta(6);
    insertarCarta(5);
    insertarCarta(4);
    insertarCarta(1);

    mostrarLista();

    printf("Después del quick\n");
    quickSort(primero);

	mostrarLista();
	return 0;
}

// Se agrega un nuevo nodo a la lista
void insertarCarta(int num){

	nodo_lista *nuevo =(nodo_lista*)malloc(sizeof(nodo_lista));
	nuevo->dato=num;
	//Esta vacía la lista
	if (primero == NULL){

		primero=nuevo;
		primero->sig = NULL;
		primero->ant = NULL;
		ultimo=primero;
	}
	//Hay un elemento ya en la lista
	else
	{
		ultimo->sig=nuevo;
		nuevo->sig=NULL;
		nuevo->ant=ultimo;
		ultimo=nuevo;
	}

}

struct nodo_lista *ultnodo(nodo_lista *raiz) 
{ 
    while (raiz && raiz->sig) 
        raiz = raiz->sig; 
    return raiz; 
} 

void swap( int* a, int* b ) {   
    int tmp = *a;      *a = *b;       *b = tmp;   
} 

nodo_lista* particion(nodo_lista* a, nodo_lista* b){
    //seleccionando pivote
    int pivote = b->dato;

    nodo_lista *ind = a->ant;

    for(nodo_lista *j = a; j != b; j = j->sig){
        if(j->dato <= ind){

            ind = ( ind == NULL)? a : ind->sig;

            swap(&(ind->dato), &(j->dato));
        }
    }
    ind = ( ind == NULL)? a : ind->sig;
    swap(&(ind->dato), &(b->dato));
    return ind;
}

void _quickSort(struct nodo *a, struct nodo *b) 
{ 
    if (b != NULL && a != b && a != b->sig){ 

        struct nodo *p = particion(a, b); 
        _quickSort(a, p->ant); 
        _quickSort(p->sig, b); 
    } 
}

void quickSort(struct nodo *cabeza) 
{ 
    // ultimo nodo
    struct nodo *h = ultnodo(cabeza); 
  
    // hace el quicksort
    _quickSort(cabeza, h); 
} 

//Muestra la lista completa doblemente enlazada
void mostrarLista(){

	nodo_lista *actual = (nodo_lista*)malloc(sizeof(nodo_lista));
	actual = primero;
    if (primero!=NULL)
	{
		int indice=1;
		while (actual != NULL)
		{
			
			printf("%d",actual->dato);
            printf("\n");

			actual= actual->sig;
			indice=indice+1;
		}
			
	}
	else
	{
		printf("\n Lista Vacía");
	}
	
}