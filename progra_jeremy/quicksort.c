// Quicksort 

#include <stdio.h>;
#include <stdlib.h>
  
// nodo doblemente enlazado

typedef struct nodo
{
    int dato;
    struct nodo *sig;
    struct nodo *ant;
    
}nodo_lista;

//se inician el primero y el ultimo
nodo_lista* primero = NULL;
nodo_lista* ultimo = NULL;
 
 int main(){
//una prueba en el main
	insertarCarta(5);
    insertarCarta(15);
    insertarCarta(4);
    insertarCarta(2);
    insertarCarta(1);

    mostrarLista();

    printf("Después del quick\n");
    //querida magia funciona
    quickSort(primero,ultimo);

	mostrarLista();
	return 0;
}

// Se agrega un nuevo nodo a la lista
void insertarCarta(int num){

	nodo_lista *nuevo =(nodo_lista*)malloc(sizeof(nodo_lista));
	nuevo->dato=num;insertarCarta(arreglo_cartas[i-1]);
	//Esta vacía la lista
	if (primero == NULL){

		primero=nuevo;
		primero->sig = NULL;
		primero->ant = NULL;
		ultimo=primero;
	}
	//Hay un elemento ya en la lista
	else
	{
		ultimo->sig=nuevo;
		nuevo->sig=NULL;
		nuevo->ant=ultimo;
		ultimo=nuevo;
	}

}

//Intercambia una posicion con otra en la lista
void swap( int* a, int* b ) {   
    int tmp = *a;      *a = *b;       *b = tmp;   
} 
//separa la lista en menores, iguales y mayores a partir del pivote
nodo_lista* particion(nodo_lista* a, nodo_lista* b){
    //seleccionando pivote será el ultimo elemento de la lista
    int pivote = b->dato;

    nodo_lista *ind = a->ant;

    for(nodo_lista *j = a; j != b; j = j->sig){
        //se mueven menores o iguales a pivote mediante swap
        if(j->dato <= pivote){
            //mueve el indice una posicion
            ind = ( ind == NULL)? a : ind->sig;
            //intercambia los nodos
            swap(&(ind->dato), &(j->dato));
        }
    }
    //mueve el indice una posicion
    ind = ( ind == NULL)? a : ind->sig;
    swap(&(ind->dato), &(b->dato));
    return ind;
}
//quicksort que retorna a sí mismo para hacer diversas particiones
void quickSort(nodo_lista *a, nodo_lista *b) {

    if (b != NULL && a != b && a != b->sig){ 

        nodo_lista *part = particion(a, b); 
        //esto hace las subdivisiones del quick
        quickSort(a, part->ant); 
        quickSort(part->sig, b); 
    } 
}

//Muestra la lista completaz doblemente enlazada
void mostrarLista(){

	nodo_lista *actual = (nodo_lista*)malloc(sizeof(nodo_lista));
	actual = primero;
    if (primero!=NULL)
	{
		int indice=1;
		while (actual != NULL)
		{
			
			printf("%d",actual->dato);
            printf("\n");

			actual= actual->sig;
			indice=indice+1;
		}
			
	}
	else
	{
		printf("\n Lista Vacía");
	}
	
}