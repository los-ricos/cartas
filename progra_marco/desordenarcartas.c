// Código para desordenar la lista de cartas de forma aleatoria

#include <stdio.h>
#include <stdlib.h>
#include "Cartas.h"
#include <time.h>

// nodo doblemente enlazado

typedef struct nodo
{
    int dato;
    struct nodo *sig;
    struct nodo *ant;
    
}nodo_lista;

nodo_lista* primero = NULL;
nodo_lista* ultimo = NULL;





 int main(){

    int i; // Variable para el for

    // Agrega las cartas
    for(i = 1; i < 53; i++){
        insertarCarta(i);

    }
    
    printf("");
    desordenar(primero,ultimo);
    mostrarLista();

	return 0;
}

// Función encargada de intercambiar posiciones en la lista
void swap( int* a, int* b ) {   
    int tmp = *a;      *a = *b;       *b = tmp;   
}

// Crea divisiones de la lista, mayores, menores e iguales
nodo_lista* particion(nodo_lista* a, nodo_lista* b){
    // Se crea el pivote como un entero 0
    int pivote = 0;
    
    nodo_lista *indice = a->ant;
    // Se crea la semilla para el random
    srand(time(0));
    

    int i = 0;
    for(nodo_lista *j = a; j != b; j = j->sig){
        //se mueven menores o iguales a pivote mediante swap
        pivote = rand() % 5; // Crea random diferentes por cada ejecución del for
        if(j->dato <= pivote*rand()){
            // Se mueve el indice una posicion
            indice = ( indice == NULL)? a : indice->sig;

            // LLama sl swap para intercambiar posicones
            swap(&(indice->dato), &(j->dato));
        }
        i++;
    }
    // Se mueve el indice una posicion
    indice = ( indice == NULL)? a : indice->sig;
    swap(&(indice->dato), &(b->dato));
    return indice;
}
// Función para desordenar la lista que realiza las particiones.
void desordenar(nodo_lista *a, nodo_lista *b) {
    

    if (b != NULL && a != b && a != b->sig){ 

        nodo_lista *division = particion(a, b);

        // Realiza las particiones
        desordenar(a, division->ant); 
        desordenar(division->sig, b); 
    } 

}

void mostrarLista(){

	nodo_lista *actual = (nodo_lista*)malloc(sizeof(nodo_lista));
	actual = primero;
    if (primero!=NULL)
	{
		int indice=1;
		while (actual != NULL)
		{
			
            printf("%d ",actual->dato);
       

			actual = actual->sig;
			indice = indice+1;
            
		}
    printf("\n");
			
	}
	else
	{
		printf("\n Lista Vacía");
	}
    
}

void insertarCarta(int num){

	nodo_lista *nuevo =(nodo_lista*)malloc(sizeof(nodo_lista));
	nuevo->dato=num;
	//Esta vacía la lista
	if (primero == NULL){

		primero=nuevo;
		primero->sig = NULL;
		primero->ant = NULL;
		ultimo=primero;
	}
	//Hay un elemento ya en la lista
	else
	{
		ultimo->sig=nuevo;
		nuevo->sig=NULL;
		nuevo->ant=ultimo;
		ultimo=nuevo;
	}

}

void CartaValor(int valor){

    switch (valor)
    {
    case 1:
        printf(BastosA);
        break;
    
    case 2:
        printf(Bastos2);
        break;
    
    case 3:
        printf(Bastos3);
        break;
    
    case 4:
        printf(Bastos4);
        break;
    
    case 5:
        printf(Bastos5);
        break;
    
    case 6:
        printf(Bastos6);
        break;    
   
    case 7:
        printf(Bastos7);
        break;    
    
    case 8:
        printf(Bastos8);
        break;
    
    case 9:
        printf(Bastos9);
        break;
    
    case 10:
        printf(Bastos10);
        break;
    
    case 11:
        printf(BastosJ);
        break;
    
    case 12:
        printf(BastosQ);
        break;
    
    case 13:
        printf(BastosK);
        break;
    
    case 14:
        printf(CorazonesA);
        break;
    
    case 15:
        printf(Corazones2);
        break;
    
    case 16:
        printf(Corazones3);
        break;
    
    case 17:
        printf(Corazones4);
        break;
    
    case 18:
        printf(Corazones5);
        break;
    
    case 19:
        printf(Corazones6);
        break;    
   
    case 20:
        printf(Corazones7);
        break;    
    
    case 21:
        printf(Corazones8);
        break;
    
    case 22:
        printf(Corazones9);
        break;
    
    case 23:
        printf(Corazones10);
        break;
    
    case 24:
        printf(CorazonesJ);
        break;
    
    case 25:
        printf(CorazonesQ);
        break;
   
    case 26:
        printf(CorazonesK);
        break;
    
    case 27:
        printf(DiamantesA);
        break;
    
    case 28:
        printf(Diamantes2);
        break;
    
    case 29:
        printf(Diamantes3);
        break;
    
    case 30:
        printf(Diamantes4);
        break;
    
    case 31:
        printf(Diamantes5);
        break;
    
    case 32:
        printf(Diamantes6);
        break;
    
    case 33:
        printf(Diamantes7);
        break;
    
    case 34:
        printf(Diamantes8);
        break;    
   
    case 35:
        printf(Diamantes9);
        break;    
    
    case 36:
        printf(Diamantes10);
        break;
    
    case 37:
        printf(DiamantesJ);
        break;
    
    case 38:
        printf(DiamantesQ);
        break;
    
    case 39:
        printf(DiamantesK);
        break;
    
    case 40:
        printf(TrebolesA);
        break;
    
    case 41:
        printf(Treboles2);
        break;
    
    case 42:
        printf(Treboles3);
        break;
    
    case 43:
        printf(Treboles4);
        break;
    
    case 44:
        printf(Treboles5);
        break;
    
    case 45:
        printf(Treboles6);
        break;
    
    case 46:
        printf(Treboles7);
        break;
    
    case 47:
        printf(Treboles8);
        break;
    
    case 48:
        printf(Treboles9);
        break;
    
    case 49:
        printf(Treboles10);
        break;    
   
    case 50:
        printf(TrebolesJ);
        break;    
    
    case 51:
        printf(TrebolesQ);
        break;
    
    case 52:
        printf(TrebolesK);
        break;

    default:
        printf(" ");
        break;
    }

}


